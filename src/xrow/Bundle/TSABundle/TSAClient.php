<?php
namespace xrow\Bundle\TSABundle;

class TSAClient extends \SoapClient
{

    private static $client = false;

    private static $sessionid = false;

    function __construct( $login = null, $password = null, $queryid = null, $wdsl = "http://ws-ni-schul.zfinder.de/V6_00/?wsdl")
    {
        $options["cache_wsdl"] = WSDL_CACHE_MEMORY;
        $options["trace"] = 1;
        $options["compression"] = SOAP_COMPRESSION_ACCEPT | SOAP_COMPRESSION_GZIP;
        
        parent::__construct( $wdsl, $options);
        if (! self::$sessionid) {
            $session = $this->startSession(array(
                "login" => $login,
                "passwort" => $password,
                "queryid" => $queryid
            ));
            self::$sessionid = $session->sessionid;
        }
    }
    public function __call($function_name, $arguments )
    {
        if ($function_name == "startSession") {
            return parent::__call($function_name, $arguments );
        }
        $arguments[0]["sessionid"] = self::$sessionid;
        try{
            $data = parent::__call($function_name, $arguments);
        }
        catch( \SoapFault $e ){
            // @TODO Implement Logger
            //$logger = $this->get('logger');
            
            echo "====== REQUEST HEADERS =====" . PHP_EOL;
            var_dump(parent::__getLastRequestHeaders());
            echo "========= REQUEST ==========" . PHP_EOL;
            var_dump(parent::__getLastRequest());
            echo "========= RESPONSE Header =========" . PHP_EOL;
            var_dump(parent::__getLastResponseHeaders());
            echo "========= RESPONSE =========" . PHP_EOL;
            var_dump(parent::__getLastResponse());

            die("TODO DEBUG VERBESSERN");
            throw $e;
        }
        /* see decode()
        if ( is_object($data) )
        {
            array_walk_recursive($data, __CLASS__ . '::decode');
        }
        */
        return $data;
    }
    /* not needed at the moment
    function decode(&$item, $key) {
    	if (is_string($item)){
    	    $item = html_entity_decode($item);
    	}
    }
    */
    // Maybe find a better way to close the seesion.
    function __destruct()
    {
        $session = $this->closeSession(array(
            "sessionid" => self::$sessionid
        ));
        self::$sessionid = false;
    }
}
