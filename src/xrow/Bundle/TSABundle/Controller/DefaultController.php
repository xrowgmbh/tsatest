<?php
namespace xrow\Bundle\TSABundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use xrow\Bundle\TSABundle\TSAClient;
use TSA\ConverterOE;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class DefaultController extends Controller
{

    const AREA_ID_WORLD = 8630828;

    const AREA_ID_GERMANY = 8630828;

    const AREA_ID_LOWER_SAXONY = 315064;

    const AREA_ID_BEZIRK = 8662364; // Bezirk Hannover/Nienburg
    const AREA_ID_REGION_HANNOVER = 8662375; // Region Hannover "Hannover"
    const AREA_ID_HANNOVER = 8663337; // Region Hannover "Hannover"
    const AREA_ID_WEDEMARK = 8663355; // Region Hannover "Wedemark"
    const AREA_ID_BARSINGHAUSEN = 8663338; // Region Hannover "Barsinghausen"
    const AREA_ID_ISERNHAGEN = 8663344; // Region Hannover "Barsinghausen"
    const AREA_ID_BURGWEDEL = 8663340; // Region Hannover "Barsinghausen"
    const AREA_ID_LEHRTE = 8663347; // Lehrte
    const ANLIEGEN_ID_HUNDESTEUER = 8664958; // Hundesteuer
    const ANLIEGEN_ID_UMMELDUNG = 8665711; // Ummeldung einer Wohnung
    const ANLIEGEN_ID_KINDERGELD_AUSZAHLUNG = 8665033; // Kindergeld Auszahlung
    const ANLIEGEN_ID_GEWERBE_ANMELDUNG = 8664844; // Gewerbeanmeldung
    const REGIONALANLIEGEN_ID_HUNDESTEUER_WEDEMARK = 9170998; // Hundesteuer Wedemark
    const ANLIEGEN_ID_FAHRERLAUBNIS_PROBE = 8669095; // Fahrerlaubnis: Erteilung - auf Probe
    const OE_ID_LHH_GEWERBEANMELDUNG = 9437834; // LHH Gewerbeanmeldung
    const OE_ID_WEDEMARK_TEAMSTEUERN = 9367686; // Gemeinde Wedemark - Team Steuern
    const OE_ID_FAMILIENKASSE_CELLE = 9;
    const OE_ID_LANDESAMT_SOZIALES = 8670016;
    const TEST_OE = 9431119; // Architektenkammer Niedersachsen
    const URL_DOKU = "http://ws-ni-schul.zfinder.de/doc/6_10/";

    /**
     * @Route("/tsa/getAnliegen")
     * @Template("xrowBundleTSABundle:tsa:demo.html.twig")
     */
    public function getAnliegen()
    {
        $client = $this->container->get('tsa.client');
        $data = $client->getAnliegen();
        foreach ($data as $li){
            foreach($li as $anliegen){
                if($anliegen->FLAGPUBLIC){
                    if(isset($anliegen->DOKUMENT)){
                        $name=$anliegen->BEZEICHNUNG;
                        var_dump($name);
                        $id=$anliegen->ID;
                        var_dump($id);
                        break;
                    }
                }
            }
        }
        #var_dump($data);
        return array(
            'data' => $data
        );
    }

    /**
     * @Route("/tsa/getAnliegenByID/{id}")
     * @Template("xrowBundleTSABundle:tsa:demo.html.twig")
     */
    public function getAnliegenByID($id)
    {
        $client = $this->container->get('tsa.client');
        $data = $client->getAnliegenByID(array(
            "id" => $id,
            "gebietsid" => self::AREA_ID_HANNOVER
        ));
        return array(
            'data' => $data
        );
    }

    /**
     * @Route("/tsa/getAnliegenGruppen")
     * @Template("xrowBundleTSABundle:tsa:demo.html.twig")
     */
    public function getAnliegenGruppen()
    {
        $client = $this->container->get('tsa.client');
        $data = $client->getAnliegenGruppen();
        return array(
            'data' => $data
        );
    }

    /**
     * @Route("/tsa/Anliegen")
     * @Template("xrowBundleTSABundle:tsa:demo.html.twig")
     */
    public function AnliegenAction()
    {
        $client = $this->container->get('tsa.client');
        $data = $client->getAnliegen(array());
        return array(
            'data' => $data
        );
    }

    /**
     * @Route("/tsa/getPersonen")
     * @Template("xrowBundleTSABundle:tsa:demo.html.twig")
     */
    public function getPersonen()
    {
        $client = $this->container->get('tsa.client');
        $data = $client->getPersonen(array());
        return array(
            'data' => $data
        );
    }    
    
    /**
     * @Route("/tsa/getAnliegenInnerhalbGebiet")
     * @Template("xrowBundleTSABundle:tsa:demo.html.twig")
     */
    public function getAnliegenInnerhalbGebiet()
    {
        $client = $this->container->get('tsa.client');
        $data = $client->getAnliegenInnerhalbGebiet(array(
            "gebietsid" => self::AREA_ID_REGION_HANNOVER
        ));
        return array(
            'data' => $data
        );
    }

    /**
     * @Route("/tsa/Synonyme")
     * @Template("xrowBundleTSABundle:tsa:demo.html.twig")
     */
    public function SynonymeAction()
    {
        $client = $this->container->get('tsa.client');
        $data = $client->getAnliegenSynonyme(array());
        return array(
            'data' => $data
        );
    }

    /**
     * @Route("/tsa/Gebiete")
     * @Template("xrowBundleTSABundle:tsa:demo.html.twig")
     */
    public function GebieteAction()
    {
        $client = $this->container->get('tsa.client');
        $data = $client->getGebiete(array(
            "suchwort" => "%" 
            #Volltextsuche" => 0
                ));
        // var_dump($data);
        return array(
            'data' => $data
        );
    }
    
    /**
     * @Route("/tsa/getGebietsTypByIDRequest")
     * @Template("xrowBundleTSABundle:tsa:demo.html.twig")
     */

    /**
     * @Route("/tsa/getGebietsTypByID/{id}")
     * @Template("xrowBundleTSABundle:tsa:demo.html.twig")
     */
    public function getGebietsTypByID($id = self::AREA_ID_REGION_HANNOVER)
    {
        $client = $this->container->get('tsa.client');
        $data = $client->getGebietsTypByID(array(
            "id" => $id
        ));
        return array(
            'data' => $data
        );
    }
    
    /**
     * @Route("/tsa/getChildrenFromGebiet/{id}")
     * @Template("xrowBundleTSABundle:tsa:demo.html.twig")
     */
    public function getChildrenFromGebiet($id = self::AREA_ID_REGION_HANNOVER)
    {
        $client = $this->container->get('tsa.client');
        $data = $client->getChildrenFromGebiet(array(
            "gebietid" => $id
        ));
        return array(
            'data' => $data
        );
    }
    
    /**
     * @Route("/tsa/getFormularByID/{id}")
     * @Template("xrowBundleTSABundle:tsa:demo.html.twig")
     */
    public function getFormularByID($id = self::AREA_ID_REGION_HANNOVER)
    {
        $client = $this->container->get('tsa.client');
        $data = $client->getFormularByID(array(
            "id" => $id
        ));
        return array(
            'data' => $data
        );
    }
    
    /**
     * @Route("/tsa/getBild/{id}")
     * @Template("xrowBundleTSABundle:tsa:demo.html.twig")
     */
    public function getBild($id = "301306098")
    {
        $client = $this->container->get('tsa.client');
        $data = $client->getBild(array(
            "id" => $id
        ));
        return array(
            'data' => $data
        );
    }
    
    
    /**
     * @Route("/tsa/getGebietByID/{id}")
     * @Template("xrowBundleTSABundle:tsa:demo.html.twig")
     */
    public function getGebietByID($id = self::AREA_ID_WORLD)
    {
        $client = $this->container->get('tsa.client');
        $data = $client->getGebietByID(array(
            "id" => $id
        ));
        return array(
            'data' => $data
        );
    }

    /**
     * @Route("/tsa/Anliegenkategorien")
     * @Template("xrowBundleTSABundle:tsa:demo.html.twig")
     */
    public function AnliegenkategorienAction()
    {
        $client = $this->container->get('tsa.client');
        $data = $client->getAnliegenkategorien(array());
        return array(
            'data' => $data
        );
    }

    /**
     * @Route("/tsa/SubAnliegenkategorien")
     * @Template("xrowBundleTSABundle:tsa:demo.html.twig")
     */
    public function SubAnliegenkategorienAction()
    {
        $client = $this->container->get('tsa.client');
        $data = $client->getSubAnliegenkategorien(array(
            "id" => "8663662"
        ));
        return array(
            'data' => $data
        );
    }

    /**
     * @Route("/tsa/getAnliegenkategorieAnliegen")
     * @Template("xrowBundleTSABundle:tsa:demo.html.twig")
     */
    public function getAnliegenkategorieAnliegen()
    {
        $client = $this->container->get('tsa.client');
        $data = $client->getAnliegenkategorieAnliegen(array(
            "id" => "8663653"
        ));
        return array(
            'data' => $data
        );
    }

    /**
     * @Route("/tsa/getAnliegenRegional")
     * @Template("xrowBundleTSABundle:tsa:demo.html.twig")
     */
    public function getAnliegenRegional()
    {
        $client = $this->container->get('tsa.client');
        $data = $client->getAnliegenRegional(array(
            "gebietid" => self::AREA_ID_BARSINGHAUSEN,
            "anliegenid" => "8665531"
        ));
        return array(
            'data' => $data
        );
    }

    /**
     * @Route("/tsa/getStandortOrganisationseinheiten")
     * @Template("xrowBundleTSABundle:tsa:demo.html.twig")
     */
    public function getStandortOrganisationseinheiten()
    {
        $client = $this->container->get('tsa.client');
        $data = $client->getStandortOrganisationseinheiten(array(
            "gebietsid" => self::AREA_ID_REGION_HANNOVER#,
            #"anliegenid" => self::ANLIEGEN_ID_KINDERGELD_AUSZAHLUNG
        ));
        
        return array(
            'data' => $data
        );
    }

    /**
     * @Route("/tsa/getOrganisationseinheitByID/{id}")
     * @Template("xrowBundleTSABundle:tsa:demo.html.twig")
     */
    public function getOrganisationseinheitByID($id = self::OE_ID_WEDEMARK_TEAMSTEUERN)
    {
        $client = $this->container->get('tsa.client');
        $data = $client->getOrganisationseinheitByID(array(
            "id" => $id
        ));
        $oe = ConverterOE::convert( $data->Organisationseinheit );
        
        var_dump($oe);
        
        return array(
            'data' => $data
        );
    }
    
    
    /**
     * @Route("/tsa/getPersonByID/{id}")
     * @Template("xrowBundleTSABundle:tsa:demo.html.twig")
     */
    public function getPersonByID($id = self::OE_ID_WEDEMARK_TEAMSTEUERN)
    {
        $client = $this->container->get('tsa.client');
        $data = $client->getPersonByID(array(
            "id" => $id
        ));
        #$oe = ConverterOE::convert( $data->Organisationseinheit );
    
        #var_dump($oe);
    
        return array(
            'data' => $data
        );
    }

    /**
     * @Route("/tsa/getDisplayOrganisationseinheitByID/{id}")
     * @Template("xrowBundleTSABundle:tsa:demo.html.twig")
     */
    public function getDisplayOrganisationseinheitByID($id = self::OE_ID_WEDEMARK_TEAMSTEUERN)
    {

        $client = $this->container->get('tsa.client');
        $data = $client->getDisplayOrganisationseinheitByID(array(
            "id" => $id
        ));

        #$oe = ConverterOE::convert( $data->Organisationseinheit );
        
        #var_dump($oe);
        return array(
            'data' => $data
        );
    }
    
    
    /**
     * @Route("/tsa/getOEHierarchien")
     * @Template("xrowBundleTSABundle:tsa:demo.html.twig")
     */
    public function getOEHierarchien()
    {
        $client = $this->container->get('tsa.client');
        $data = $client->getOEHierarchien();
        
        return array(
            'data' => $data
        );
    }
    // eigt sich und die Kinder bei false
    /**
     * @Route("/tsa/TreeOrganisationseinheiten")
     * @Template("xrowBundleTSABundle:tsa:demo.html.twig")
     */
    public function TreeOrganisationseinheiten()
    {
        $client = $this->container->get('tsa.client');
        $data = $client->getTreeOrganisationseinheiten(array(
            "id" => "9437981",
            "complete" => false
        ));
        return array(
            'data' => $data
        );
    }

    /**
     * @Route("/tsa/getAnliegenBlocktypen")
     * @Template("xrowBundleTSABundle:tsa:demo.html.twig")
     */
    public function getAnliegenBlocktypen()
    {
        $client = $this->container->get('tsa.client');
        
        // Use Client
        $blocktypen = $client->getAnliegenBlocktypen();
        foreach($blocktypen->AnliegenBlocktyp as $block){
            echo "<li><a>{$block->BEZEICHNUNG}</a></li>";
        }
        return array(
            'data' => $blocktypen
        );
    }

    /**
     * @Route("/tsa/getZstOrganisationseinheiten")
     * @Template("xrowBundleTSABundle:tsa:demo.html.twig")
     */
    public function getZstOrganisationseinheiten()
    {
        $client = $this->container->get('tsa.client');
        
        // Use Client
        $anliegen = $client->getZstOrganisationseinheiten(array(
            "gebietsid" => self::AREA_ID_HANNOVER,
            "anliegenid" => 
             #self::ANLIEGEN_ID_GEWERBE_ANMELDUNG
            "8663863" // Architekten
        ));
        return array(
            'data' => $anliegen
        );
    }

    /**
     * @Route("/tsa/getBeispielOrganisationseinheiten")
     * @Template("xrowBundleTSABundle:tsa:demo.html.twig")
     */
    public function getBeispielOrganisationseinheiten()
    {
        $client = $this->container->get('tsa.client');
    
        // Use Client
        $anliegen = $client->getAnliegen(array());
        foreach ($anliegen as $li){
            foreach($li as $anliegen){
                if($anliegen->FLAGPUBLIC){
                    $id=$anliegen->ID;
                    $organisationen = $client->getZstOrganisationseinheiten(array(
                        "gebietsid" => self::AREA_ID_HANNOVER,
                        "anliegenid" => $id
                    ));
                    #var_dump($organisationen);
                    if(isset($organisationen->Organisationseinheit) and is_object($organisationen->Organisationseinheit)){
                        $organisationen = array( $organisationen->Organisationseinheit );
                    }
                    elseif( isset($organisationen->Organisationseinheit) and is_array($organisationen->Organisationseinheit)){
                        $organisationen = $organisationen->Organisationseinheit;
                    }
                    if(is_array($organisationen)){
                        foreach($organisationen as $organisation){
                            #if(isset($organisation->FLAGEARELEVANZ) and $organisation->FLAGEARELEVANZ == true){
                            #   $name=$organisation->BEZEICHNUNG;
                                $id=$organisation->ID;
                                echo "{$id}<br>";
                            #    echo"is FLAGEARELEVANZ {$name} {$id}";
                            #}else{
                                if(isset($organisation->E_ANTRAG_ANLIEGENID)){
                                    $name=$organisation->BEZEICHNUNG;
                                    $id=$organisation->ID;
                                    echo"is E_ANTRAG_ANLIEGENID {$name} {$id}";
                                    exit();
                                }
                            #}
                        }
                    }
                }
            }
        }

        return array(
            'data' => $anliegen
        );
    }
    
    /**
     * @Route("/tsa/getOrganisationseinheitFormulare")
     * @Template("xrowBundleTSABundle:tsa:demo.html.twig")
     */
    public function getOrganisationseinheitFormulare()
    {
        $client = $this->container->get('tsa.client');
        
        // Use Client
        $anliegen = $client->getOrganisationseinheitFormulare(array(
            "organisationseinheitid" => self::OE_ID_LHH_GEWERBEANMELDUNG,
            "anliegenid" => self::ANLIEGEN_ID_GEWERBE_ANMELDUNG
        ));
        return array(
            'data' => $anliegen
        );
    }

    /**
     * @Route("/tsa/getZstDisplayOrganisationseinheiten")
     * @Template("xrowBundleTSABundle:tsa:demo.html.twig")
     */
    public function getZstDisplayOrganisationseinheiten()
    {
        $client = $this->container->get('tsa.client');
        
        // Use Client
        $anliegen = $client->getZstDisplayOrganisationseinheiten(array(
            "gebietsid" => self::AREA_ID_BURGWEDEL,
            "anliegenid" => "312763853"
            #self::ANLIEGEN_ID_KINDERGELD_AUSZAHLUNG
        ));
        return array(
            'data' => $anliegen
        );
    }

    /**
     * @Route("/tsa/getOrganisationseinheitAnliegen")
     * @Template("xrowBundleTSABundle:tsa:demo.html.twig")
     */
    public function getOrganisationseinheitAnliegen()
    {
        $client = $this->container->get('tsa.client');
        
        // Use Client
        $anliegen = $client->getOrganisationseinheitAnliegen(array(
            "organisationseinheitid" => self::OE_ID_LHH_GEWERBEANMELDUNG,
            "nuroeffentlich" => true
        ));
        return array(
            'data' => $anliegen
        );
    }

    /**
     * @Route("/tsa/getOrganisationseinheitMitarbeiter")
     * @Template("xrowBundleTSABundle:tsa:demo.html.twig")
     */
    public function getOrganisationseinheitMitarbeiter()
    {
        $client = $this->container->get('tsa.client');
        
        // Use Client
        $anliegen = $client->getOrganisationseinheitMitarbeiter(array(
            "organisationseinheitid" => self::OE_ID_WEDEMARK_TEAMSTEUERN,
            "anliegenids" => self::ANLIEGEN_ID_HUNDESTEUER
        ));
        return array(
            'data' => $anliegen
        );
    }

    /**
     * @Route("/tsa/getAdressTypen")
     * @Template("xrowBundleTSABundle:tsa:demo.html.twig")
     */
    public function getAdressTypen()
    {
        $client = $this->container->get('tsa.client');
        
        // Use Client
        $anliegen = $client->getAdressTypen();
        return array(
            'data' => $anliegen
        );
    }

    /**
     * @Route("/tsa/getLinienTypen")
     * @Template("xrowBundleTSABundle:tsa:demo.html.twig")
     */
    public function getLinienTypen()
    {
        $client = $this->container->get('tsa.client');
        
        // Use Client
        $anliegen = $client->getLinienTypen();
        return array(
            'data' => $anliegen
        );
    }
    
    /**
     * @Route("/tsa/getParkplatzTypen")
     * @Template("xrowBundleTSABundle:tsa:demo.html.twig")
     */
    public function getParkplatzTypen()
    {
        $client = $this->container->get('tsa.client');
    
        // Use Client
        $anliegen = $client->getParkplatzTypen();
        return array(
            'data' => $anliegen
        );
    }
    
    /**
     * @Route("/tsa/getINetAdressTypen")
     * @Template("xrowBundleTSABundle:tsa:demo.html.twig")
     */
    public function getINetAdressTypen()
    {
        $client = $this->container->get('tsa.client');
    
        // Use Client
        $anliegen = $client->getINetAdressTypen();
        return array(
            'data' => $anliegen
        );
    }

    /**
     * @Route("/tsa/getOEKategorien")
     * @Template("xrowBundleTSABundle:tsa:demo.html.twig")
     */
    public function getOEKategorien()
    {
        $client = $this->container->get('tsa.client');
        
        // Use Client
        $anliegen = $client->getOEKategorien();
        return array(
            'data' => $anliegen
        );
    }

    /**
     * @Route("/tsa/getKontaktsystemTypen")
     * @Template("xrowBundleTSABundle:tsa:demo.html.twig")
     */
    public function getKontaktsystemTypen()
    {
        $client = $this->container->get('tsa.client');
        
        // Use Client
        $anliegen = $client->getKontaktsystemTypen();
        return array(
            'data' => $anliegen
        );
    }

    /**
     * @Route("/tsa/getVerrichtungen")
     * @Template("xrowBundleTSABundle:tsa:demo.html.twig")
     */
    public function getVerrichtungen()
    {
        $client = $this->container->get('tsa.client');
        
        // Use Client
        $anliegen = $client->getVerrichtungen();
        return array(
            'data' => $anliegen
        );
    }

    /**
     * @Route("/tsa/getAnliegenLOVZbyAnliegenLO")
     * @Template("xrowBundleTSABundle:tsa:demo.html.twig")
     */
    public function getAnliegenLOVZbyAnliegenLO()
    {
        $client = $this->container->get('tsa.client');
        
        // Use Client
        $anliegen = $client->getAnliegenLOVZbyAnliegenLO(array(
            // organisationseinheitid" => self::OE_ID_WEDEMARK_TEAMSTEUERN,
            "id" => "307986784"
        ));
        return array(
            'data' => $anliegen
        );
    }
    
    /**
     * @Route("/tsa/getElektronischeAntragstellungLink")
     * @Template("xrowBundleTSABundle:tsa:demo.html.twig")
     */
    public function getElektronischeAntragstellungLink()
    {
        $client = $this->container->get('tsa.client');
    
        // Use Client
        $anliegen = $client->getElektronischeAntragstellungLink(array(
            // organisationseinheitid" => self::OE_ID_WEDEMARK_TEAMSTEUERN,
            "AnliegenID" => "9335249",
            "GebietID" => self::AREA_ID_HANNOVER
        ));
        return array(
            'data' => $anliegen
        );
    }
    

    /**
     * @Route("/tsa/sucheanliegen/{suchwort}")
     * @Template("xrowBundleTSABundle:tsa:demo.html.twig")
     */
    public function sucheAnliegen($suchwort = "Personalausweis")
    {
        $client = $this->container->get('tsa.client');
        
        // Use Client
        $anliegen = $client->getAnliegen(array(
            "suchwort" => $suchwort,
            "Volltextsuche" => 0
        ));
        return array(
            'data' => $anliegen
        );
    }
    
    /**
     * @Route("/tsa/getFachlicheZustaendigkeiten")
     * @Template("xrowBundleTSABundle:tsa:demo.html.twig")
     */
    public function getFachlicheZustaendigkeiten()
    {
        $client = $this->container->get('tsa.client');
    
        // Use Client
        $anliegen = $client->getFachlicheZustaendigkeiten();
        return array(
            'data' => $anliegen
        );
    }
    
    /**
     * @Route("/tsa/getDokumentTypen")
     * @Template("xrowBundleTSABundle:tsa:demo.html.twig")
     */
    public function getDokumentTypen()
    {
        $client = $this->container->get('tsa.client');
    
        // Use Client
        $anliegen = $client->getDokumentTypen();
        return array(
            'data' => $anliegen
        );
    }
    
    /**
     * @Route("/tsa/getDokumentSchluessel")
     * @Template("xrowBundleTSABundle:tsa:demo.html.twig")
     */
    public function getDokumentSchluessel()
    {
        $client = $this->container->get('tsa.client');
    
        // Use Client
        $data = $client->getDokumentSchluessel();
        foreach ($data as $li){
            foreach($li as $DokumentSchluessel){
                if(isset($DokumentSchluessel->ID)and $DokumentSchluessel->ID == 11904613){
                    $id=$DokumentSchluessel->ID;
                    var_dump($id);
                    $bezeichnung=$DokumentSchluessel->BEZEICHNUNG;
                    var_dump($bezeichnung);
                    break;
                }
            }
        }
        #var_dump($data);
        return array(
            'data' => $data
        );
    }
    

    /**
     * @Route("/tsa/getGebuehrSchluessel")
     * @Template("xrowBundleTSABundle:tsa:demo.html.twig")
     */
    public function getGebuehrSchluessel()
    {
        $client = $this->container->get('tsa.client');
    
        // Use Client
        $data = $client->getGebuehrSchluessel();
        foreach ($data as $li){
            foreach($li as $GebuehrSchluessel){
                if(isset($GebuehrSchluessel->ID)and $GebuehrSchluessel->ID == 9235557){
                    $id=$GebuehrSchluessel->ID;
                    var_dump($id);
                    $bezeichnung=$GebuehrSchluessel->BEZEICHNUNG;
                    var_dump($bezeichnung);
                    break;
                }
            }
        }
        #var_dump($data);
        return array(
        'data' => $data
        );
    }
    
    
    /**
     * @Route("/tsa/getFristSchluessel")
     * @Template("xrowBundleTSABundle:tsa:demo.html.twig")
     */
    public function getFristSchluessel()
    {
        $client = $this->container->get('tsa.client');
    
        // Use Client
        $data = $client->getFristSchluessel();
        foreach ($data as $li){
            foreach($li as $FristSchluessel){
                if(isset($FristSchluessel->ID)and $FristSchluessel->ID == 9235576){
                    $id=$FristSchluessel->ID;
                    var_dump($id);
                    $bezeichnung=$FristSchluessel->BEZEICHNUNG;
                    var_dump($bezeichnung);
                    break;
                }
            }
        }
        #var_dump($data);
        return array(
        'data' => $data
        );
    }
    

    /**
     * @Route("/tsa/getBaseObjectByID/{id}")
     * @Template("xrowBundleTSABundle:tsa:demo.html.twig")
     */
    public function getBaseObjectByID($id)
    {
        $client = $this->container->get('tsa.client');
        $data = $client->getBaseObjectByID(array(
            "id" => $id,
            #"gebietsid" => self::AREA_ID_HANNOVER
        ));
        return array(
            'data' => $data
        );
    }
    
    /**
     * @Route("/tsa/getBaseObjectsByFunction")
     * @Template("xrowBundleTSABundle:tsa:demo.html.twig")
     */
    public function getBaseObjectsByFunction()
    {
        $client = $this->container->get('tsa.client');
        $data = $client->getBaseObjectsByFunction(array(
            'functionname' => 'getAnliegen',
            # Defect !
            'functionparameter' => array(array("NAME" => 'id', "VALUE"=> '8669140' ))

        ));
        return array(
            'data' => $data
        );
    }
    
    /**
     * @Route("/tsa/getTelefonTypen")
     * @Template("xrowBundleTSABundle:tsa:demo.html.twig")
     */
    public function getTelefonTypen()
    {
        $client = $this->container->get('tsa.client');
        $data = $client->getTelefonTypen( );
        return array(
            'data' => $data
        );
    }
    
    /**
     * @Route("/tsa/case1")
     * @Template("xrowBundleTSABundle:tsa:demo.html.twig")
     */
    public function case1Action()
    {
        $client = $this->container->get('tsa.client');
        
        // Use Client
        $anliegen = $client->getAnliegen(array(
            "suchwort" => "Hundesteuer Festsetzung",
            "Volltextsuche" => 0
        ));
        return array(
            'data' => $anliegen
        );
    }
}
