<?php

namespace TSA\Models;

class Organisation {
    public $id;
    public $name;
    public $superior_organisation;
    public $image;
    public $contact_person;
    public $email;
    public $phone_one;
    public $phone_two;
    public $fax_number_one;
    public $fax_number_two;
    public $url;
    public $url_text;
    public $xrowgis;
    public $pobox;
    public $zip_pobox;
    public $description;
    public $metadata;
    public $forms;
}