<?php
namespace TSA;

use TSA\Models\Organisation;

class ConverterOE
{
    
    CONST TYP_TELEFON = "183669";

    CONST TYP_FAX = "183670";

    CONST TYP_MOBILE = "183671";

    CONST TYP_EMAIL = "183675";
    
    CONST TYP_WWW = "183674";
    
    CONST TYP_POSTADRESSE = "183672";
    
    CONST TYP_POSTFACH = "183673";

    CONST TYP_BESUCHERADRESSE = "200167";
   
    /*
     * @throws Exception if conversion fails @return TSA\Models\Organisation
     */
    public static function convert($oe)
    {
        #$contentType = $contentTypeService->loadContentTypeByIdentifier( 'organisation' );
        #$contentStruct = $contentService->newContentCreateStruct( $contentType, 'ger-DE' );

        $newoe = new Organisation();
        // id
        $newoe->id = $oe->ID;
        // name
        if (isset($oe->BEZEICHNUNG)) {
        $newoe->name = $oe->BEZEICHNUNG;
        }
        // superior_organisation
        if (isset($oe->PARENTID)) {
            $newoe->superior_organisation = $oe->PARENTID;                  //TODO: Objekt Organisation muss erzeugt werden
        }
        // image (ID)
        if (isset($oe->BILD)) {
            if (is_array($oe->BILD)) {
                foreach ($oe->BILD as $bild) {
                    if (isset($bild->HAUPTBILD) and $bild->HAUPTBILD == true){
                        $newoe->image = $bild->ID;                          //TODO: Objekt Bild muss erzeugt werden
                        break;
                    }
                }
            }
            else{
                $newoe->image = $oe->BILD->ID;
            }
        }
        // contact_person
        if (isset($oe->PERSON)) {
            if (is_array($oe->PERSON)) {
                foreach ($oe->PERSON as $person) {
                    if(isset($person->FLAGSHOW) and $person->FLAGSHOW == true){
                        $newoe->contact_person[] = $person->NACHNAME;       //TODO: Objekt Kontakt muss erzeugt werden
                    }
                }
            }
            else{
                if (isset($oe->PERSON->FLAGSHOW) and $oe->PERSON->FLAGSHOW == true) {
                    $newoe->contact_person = $oe->PERSON->NACHNAME;        //TODO: Objekt Kontakt muss erzeugt werden
                }
            }
        }
        // email, url
        if (isset($oe->INETADRESSE)) {
            if (is_array($oe->INETADRESSE)) {
                foreach ($oe->INETADRESSE as $inetadresse) {
                    if ($inetadresse->TYPID == self::TYP_EMAIL and !isset($newoe->email)) {
                        $newoe->email = $inetadresse->URL;
                    }
                    elseif ($inetadresse->TYPID == self::TYP_WWW and !isset($newoe->url)){
                        $newoe->url = $inetadresse->URL;
                        if (isset($inetadresse->TITLE)){
                            $newoe->url_text= $inetadresse->TITLE;
                        }
                    }
                }
            }
            else{
                if ($oe->INETADRESSE->TYPID == self::TYP_EMAIL) {
                    $newoe->email = $oe->INETADRESSE->URL;
                }
                elseif ($oe->INETADRESSE->TYPID == self::TYP_WWW){
                    $newoe->url = $oe->INETADRESSE->URL;
                    if (isset($oe->INETADRESSE->TITLE)){
                        $newoe->url_text= $oe->INETADRESSE->TITLE;
                    }
                }
            }
        }
        // phone_one, phone_two, fax_number_one, fax_number_two
        if (isset($oe->TELEFON)) {
            if (is_array($oe->TELEFON)) {
                foreach ($oe->TELEFON as $telefon) {
                    if ($telefon->TYPID == self::TYP_TELEFON and !isset($newoe->phone_one)) {
                        $newoe->phone_one = $telefon->NUMMER;
                    }
                    elseif($telefon->TYPID == self::TYP_TELEFON and isset($newoe->phone_one)) {
                        $newoe->phone_two = $telefon->NUMMER;
                    }
                    elseif($telefon->TYPID == self::TYP_FAX and !isset($newoe->fax_number_one)) {
                        $newoe->fax_number_one = $telefon->NUMMER;
                    }
                    elseif($telefon->TYPID == self::TYP_FAX and isset($newoe->fax_number_one)) {
                          $newoe->fax_number_two = $telefon->NUMMER;
                    }
                }
            }
            else{
                if ($oe->TELEFON->TYPID == self::TYP_TELEFON) {
                    $newoe->phone_one = $oe->TELEFON->NUMMER;
                }
                elseif ($oe->TELEFON->TYPID == self::TYP_FAX) {
                    $newoe->fax_number_one = $oe->TELEFON->NUMMER;
                }
            }
        }
        //xrowgis, pobox, zip_pobox
        if (isset($oe->ADRESSE)) {
            if (is_array($oe->ADRESSE)) {
                $typ ="";
                foreach ($oe->ADRESSE as $adresse) {
                    if ($adresse->TYPID == self::TYP_BESUCHERADRESSE and $typ != self::TYP_BESUCHERADRESSE) {
                        $typ = self::TYP_BESUCHERADRESSE;  
                        if (isset($adresse->PLZ)){
                            $newoe->xrowgis["zip"] = $adresse->PLZ;
                        }
                        if (isset($adresse->STRASSE)){
                            $newoe->xrowgis["street"] = $adresse->STRASSE;
                        }
                        if (isset($adresse->HAUSNUMMER)){
                            $newoe->xrowgis["street"] .=" {$adresse->HAUSNUMMER}";
                        }
                        #if (isset($adresse->ORTID)){                                               //TODO: getGebietByID
                        #$newoe->xrowgis["city"] = $adresse->PLZ;
                        #}
                        if (isset($adresse->GEO->KOORDINATE1) and isset($adresse->GEO->KOORDINATE2)){
                        $newoe->xrowgis["longitude"] = $adresse->GEO->KOORDINATE1;                  //TODO: Es gibt unterschiedliche Koordinatensystem. Bei 8
                        $newoe->xrowgis["latitude"] = $adresse->GEO->KOORDINATE2;
                        }
                        $newoe->xrowgis["country"] = "Germany";
                    }
                    elseif($adresse->TYPID == self::TYP_POSTADRESSE and !isset($newoe->xrowgis)) {
                        if (isset($adresse->PLZ)){
                            $newoe->xrowgis["zip"] = $adresse->PLZ;
                        }
                        if (isset($adresse->STRASSE)){
                            $newoe->xrowgis["street"] = $adresse->STRASSE;
                        }
                        if (isset($adresse->HAUSNUMMER)){
                            $newoe->xrowgis["street"] .=" {$adresse->HAUSNUMMER}";
                        }
                        #if (isset($adresse->ORTID)){                                               //TODO: getGebietByID
                        #$newoe->xrowgis["city"] = $adresse->PLZ;
                        #}
                        if (isset($adresse->GEO->KOORDINATE1) and isset($adresse->GEO->KOORDINATE2)){
                        $newoe->xrowgis["longitude"] = $adresse->GEO->KOORDINATE1;                  //TODO: Es gibt unterschiedliche Koordinatensystem. Bei 8
                        $newoe->xrowgis["latitude"] = $adresse->GEO->KOORDINATE2;
                        }
                        $newoe->xrowgis["country"] = "Germany";
                    }
                    elseif($adresse->TYPID == self::TYP_POSTFACH and !isset($newoe->pobox) and !isset($newoe->zip_pobox)) {
                        if (isset($adresse->PLZ)){
                        $newoe->zip_pobox = $adresse->PLZ;
                        }
                        if (isset($adresse->POSTFACH)){
                        $newoe->pobox = $adresse->POSTFACH;
                        }
                    }
                }
            }
            else{
                $typ ="";
                if ($oe->ADRESSE->TYPID == self::TYP_BESUCHERADRESSE and $typ != self::TYP_BESUCHERADRESSE) {
                    $typ = self::TYP_BESUCHERADRESSE;  
                    if (isset($oe->ADRESSE->PLZ)){
                    $newoe->xrowgis["zip"] = $oe->ADRESSE->PLZ;
                    }
                    if (isset($oe->ADRESSE->STRASSE)){
                        $newoe->xrowgis["street"] = $oe->ADRESSE->STRASSE;
                    }
                    if (isset($oe->ADRESSE->HAUSNUMMER)){
                        $newoe->xrowgis["street"] .=" {$oe->ADRESSE->HAUSNUMMER}";
                    }
                    #if (isset($oe->ADRESSE->ORTID)){
                    #$newoe->xrowgis["city"] = $oe->ADRESSE->PLZ;                                     //TODO: getGebietByID
                    #}
                    if (isset($oe->ADRESSE->KOORDINATE1) and isset($oe->ADRESSE->KOORDINATE2)){
                    $newoe->xrowgis["longitude"] = $oe->ADRESSE->GEO->KOORDINATE1;
                    $newoe->xrowgis["latitude"] = $oe->ADRESSE->GEO->KOORDINATE2;
                    }
                    $newoe->xrowgis["country"] = "Germany";
                }
                elseif($oe->ADRESSE->TYPID == self::TYP_POSTADRESSE and !isset($newoe->xrowgis)) {
                                    if (isset($oe->ADRESSE->PLZ)){
                    $newoe->xrowgis["zip"] = $oe->ADRESSE->PLZ;
                    }
                    if (isset($oe->ADRESSE->STRASSE)){
                        $newoe->xrowgis["street"] = $oe->ADRESSE->STRASSE;
                    }
                    if (isset($oe->ADRESSE->HAUSNUMMER)){
                        $newoe->xrowgis["street"] .=" {$oe->ADRESSE->HAUSNUMMER}";
                    }
                    #if (isset($oe->ADRESSE->ORTID)){
                    #$newoe->xrowgis["city"] = $oe->ADRESSE->PLZ;                                     //TODO: getGebietByID
                    #}
                    if (isset($oe->ADRESSE->KOORDINATE1) and isset($oe->ADRESSE->KOORDINATE2)){
                    $newoe->xrowgis["longitude"] = $oe->ADRESSE->GEO->KOORDINATE1;
                    $newoe->xrowgis["latitude"] = $oe->ADRESSE->GEO->KOORDINATE2;
                    }
                    $newoe->xrowgis["country"] = "Germany";
                }
                elseif($oe->ADRESSE->TYPID == self::TYP_POSTFACH and !isset($newoe->pobox) and !isset($newoe->zip_pobox) ) {
                    if (isset($oe->ADRESSE->PLZ)){
                    $newoe->zip_pobox = $oe->ADRESSE->PLZ;
                    }
                    if (isset($oe->ADRESSE->POSTFACH)){
                    $newoe->pobox = $oe->ADRESSE->POSTFACH;
                    }
                }
            }
        }
        return $newoe;
    }
}