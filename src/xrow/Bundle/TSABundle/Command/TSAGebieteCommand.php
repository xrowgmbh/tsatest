<?php
namespace xrow\Bundle\TSABundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use xrow\Bundle\TSABundle\TSAClient;

class TSAGebieteCommand extends ContainerAwareCommand
{
    const AREA_ID_WORLD = 8630828;
    
    const AREA_ID_GERMANY = 8630828;
    
    const AREA_ID_LOWER_SAXONY = 315064;
    
    const AREA_ID_BEZIRK = 8662364; // Bezirk Hannover/Nienburg
    const AREA_ID_REGION_HANNOVER = 8662375; // Region Hannover 
    const AREA_ID_HANNOVER = 8663337; // "Hannover"
    const AREA_ID_WEDEMARK = 8663355; // "Wedemark"
    const AREA_ID_BARSINGHAUSEN = 8663338; // "Barsinghausen"
    const ANLIEGEN_ID_HUNDESTEUER = 8664958; // Hundesteuer
    const ANLIEGEN_ID_UMMELDUNG = 8665711; // Ummeldung einer Wohnung
    const ANLIEGEN_ID_KINDERGELD_AUSZAHLUNG = 8665033; // Kindergeld Auszahlung
    const ANLIEGEN_ID_GEWERBE_ANMELDUNG = 8664844; // Gewerbeanmeldung
    const REGIONALANLIEGEN_ID_HUNDESTEUER_WEDEMARK = 9170998; // Hundesteuer Wedemark
    const ANLIEGEN_ID_FAHRERLAUBNIS_PROBE = 8669095; // Fahrerlaubnis: Erteilung - auf Probe
    const OE_ID_LHH_GEWERBEANMELDUNG = 9437834; // LHH Gewerbeanmeldung
    const OE_ID_WEDEMARK_TEAMSTEUERN = 9367686; // Gemeinde Wedemark - Team Steuern
    const OE_ID_FAMILIENKASSE_CELLE = 9;
    const OE_ID_LANDESAMT_SOZIALES = 8670016;
    const TEST_OE = 9431119; // Architektenkammer Niedersachsen
    const URL_DOKU = "http://ws-ni-schul.zfinder.de/doc/6_00/";
    
    protected function configure()
    {
        $this->setName('tsa:gebiete')
            ->setDescription('This is a demo script to get some TSA demo data');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $client = $this->getContainer()->get('tsa.client');

        // Use Client
        $id_oe_all = array();
        $gebiete_all = $client->getChildrenFromGebiet(array(
            "gebietid" => self::AREA_ID_REGION_HANNOVER
        ));
        $anliegen = $client->getAnliegen(array());
        foreach ($gebiete_all->Gebiet as $gebiet){
            $g_id= $gebiet->ID;
            echo "{$gebiet->BEZEICHNUNG} \n";
            foreach ($anliegen as $li){
                foreach($li as $anliegen_li){
                    if($anliegen_li->FLAGPUBLIC){
                        $a_id=$anliegen_li->ID;
                        #ANLIEGEN TESTEN
                        /*
                        if(isset($anliegen_li->FRIST) and is_object($anliegen_li->FRIST)){
                            $frist = array( $anliegen_li->FRIST );
                        }
                        elseif( isset($anliegen_li->FRIST) and is_array($anliegen_li->FRIST)){
                            $frist = $anliegen_li->FRIST;
                        }
                        if(isset($frist)and !empty($frist)){
                            foreach($frist as $frist_li){
                                if(isset($frist_li->INTERVALLDATUM) and !empty($frist_li->INTERVALLDATUM)){
                                    echo "{$a_id}\n";
                                }
                            }
                        }*/
                        # REGIONALANLIEGEN TESTEN
                        $anliegenregional = $client->getAnliegenRegional(array(
                            "gebietid" => $g_id,
                            "anliegenid" => $a_id
                        ));
                        if(isset($anliegenregional) and !empty($anliegenregional)){
                            $count=0;
                            foreach($anliegenregional as $anliegenregional_li){
                                $count=+1;
                                if($count > 1){
                                    echo "more then one {$a_id} and {$g_id} \n" ;
                                }
                                /*if(isset($anliegenregional_li->FRIST) and !empty($anliegenregional_li->FRIST)){
                                    if(isset($anliegenregional_li->FRIST) and is_object($anliegenregional_li->FRIST)){
                                        $verwaltungsfrist = array( $anliegenregional_li->FRIST );
                                    }
                                    elseif( isset($anliegenregional_li->FRIST) and is_array($anliegenregional_li->FRIST)){
                                        $verwaltungsfrist = $anliegenregional_li->FRIST;
                                    }
                                    if(isset($verwaltungsfrist)){
                                        #foreach($verwaltungsfrist as $ver_frist){
                                        #    if(isset($ver_frist->GUELTIGKEITSDATUMENDE) and !empty($ver_frist->GUELTIGKEITSDATUMENDE)){
                                                echo "{$a_id}\n";
                                        #    }
                                        #}
                                    }
                                }*/
                            }
                        }
                            # OE TESTEN 
                            /*$organisationen = $client->getZstDisplayOrganisationseinheiten(array(
                                "gebietsid" => $gebiet->ID,
                                "anliegenid" => $a_id
                            ));
                            #var_dump($organisationen);
                            if(isset($organisationen->Organisationseinheit) and is_object($organisationen->Organisationseinheit)){
                                $organisationen = array( $organisationen->Organisationseinheit );
                            }
                            elseif( isset($organisationen->Organisationseinheit) and is_array($organisationen->Organisationseinheit)){
                                $organisationen = $organisationen->Organisationseinheit;
                            }
                            if(is_array($organisationen)){
                                foreach($organisationen as $organisation){
                                    if(!in_array($organisation->ID, $id_oe_all)){
                                        $id_oe_all[]=$organisation->ID;
                                    
                                    if(isset($organisation->FSERLAUBEAUTOFORM) and ($organisation->FSERLAUBEAUTOFORM == false) and isset($organisation->FORMULARZENTRALID)){
                                        $name=$organisation->BEZEICHNUNG;
                                        $id_oe=$organisation->ID;
                                        echo"is FLAGEARELEVANZ {$name} {$id_oe}\n";
                                    }#else{
                                    # PERSONEN TESTEN
                                        /*if(isset($organisation->PERSONLOKAL) and !empty($organisation->PERSONLOKAL)){
                                            $name=$organisation->BEZEICHNUNG;
                                            $id_oe=$organisation->ID;
                                            #echo "{$name}\n";
                                            $personen= $organisation->PERSONLOKAL;
                                            if(!is_array($personen)){
                                                $personen_array = array( $personen );
                                            }
                                            else{
                                                $personen_array = $personen;
                                            }
                                            foreach($personen_array as $lokalperson){
                                                $id_person=$lokalperson->PERSONID;
                                                $person = $client->getPersonByID(array(
                                                    "id" => $id_person,
                                                ));
                                                if(isset($person->Person->TITEL) and !empty($person->Person->TITEL)){
                                                    $name=$person->Person->NACHNAME;
                                                    echo "has Titel {$name} {$id_person}\n";
                                                }
                                                if(isset($person->Person->FLAGANONYM) and $person->Person->FLAGANONYM == true){
                                                    $name=$person->Person->NACHNAME;
                                                    echo "has FLAGANONYM true {$name} {$id_person}\n";
                                                }
                                                if(isset($person->Person->FLAGSHOW) and $person->Person->FLAGSHOW == false){
                                                    $name=$person->Person->NACHNAME;
                                                    echo "has FLAGSHOW false {$name} {$id_person}\n";
                                                }
                                                if(isset($person->Person->SONSTIGEANGABEN) and !empty($person->Person->SONSTIGEANGABEN)){
                                                    $name=$person->Person->NACHNAME;
                                                    echo "has SONSTIGEANGABEN {$name} {$id_person}\n";
                                                }
                                                if(isset($person->Person->BILD) and !empty($person->Person->BILD)){
                                                    $name=$person->Person->NACHNAME;
                                                    echo "has BILD {$name} {$id_person}\n";
                                                }
                                            }
                                        }*/
                                    #}
                                #}
                            #}
                        }
                    }
                }
            }
            #$count = count($id_oe_all);
            #echo "{$count}\n";
        #}
        #$count = count($id_oe_all);
        #echo "Final: {$count}";
        return array(
            'data' => $anliegen
        );
    }
}
