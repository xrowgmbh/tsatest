<?php
namespace xrow\Bundle\TSABundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use xrow\Bundle\TSABundle\TSAClient;

class TSAAnliegenBeispielCommand extends ContainerAwareCommand
{
    const AREA_ID_WORLD = 8630828;
    
    const AREA_ID_GERMANY = 8630828;
    
    const AREA_ID_LOWER_SAXONY = 315064;
    
    const AREA_ID_BEZIRK = 8662364; // Bezirk Hannover/Nienburg
    const AREA_ID_REGION_HANNOVER = 8662375; // Region Hannover "Hannover"
    const AREA_ID_HANNOVER = 8663337; // Region Hannover "Hannover"
    const AREA_ID_WEDEMARK = 8663355; // Region Hannover "Wedemark"
    const AREA_ID_BARSINGHAUSEN = 8663338; // Region Hannover "Barsinghausen"
    const ANLIEGEN_ID_HUNDESTEUER = 8664958; // Hundesteuer
    const ANLIEGEN_ID_UMMELDUNG = 8665711; // Ummeldung einer Wohnung
    const ANLIEGEN_ID_KINDERGELD_AUSZAHLUNG = 8665033; // Kindergeld Auszahlung
    const ANLIEGEN_ID_GEWERBE_ANMELDUNG = 8664844; // Gewerbeanmeldung
    const REGIONALANLIEGEN_ID_HUNDESTEUER_WEDEMARK = 9170998; // Hundesteuer Wedemark
    const ANLIEGEN_ID_FAHRERLAUBNIS_PROBE = 8669095; // Fahrerlaubnis: Erteilung - auf Probe
    const OE_ID_LHH_GEWERBEANMELDUNG = 9437834; // LHH Gewerbeanmeldung
    const OE_ID_WEDEMARK_TEAMSTEUERN = 9367686; // Gemeinde Wedemark - Team Steuern
    const OE_ID_FAMILIENKASSE_CELLE = 9;
    const OE_ID_LANDESAMT_SOZIALES = 8670016;
    const TEST_OE = 9431119; // Architektenkammer Niedersachsen
    const URL_DOKU = "http://ws-ni-schul.zfinder.de/doc/6_00/";
    
    protected function configure()
    {
        $this->setName('tsa:anliegen')
            ->setDescription('This is a demo script to get some TSA demo data');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $client = $this->getContainer()->get('tsa.client');

        // Use Client
        $gebiete_all = $client->getChildrenFromGebiet(array(
            "gebietid" => self::AREA_ID_REGION_HANNOVER
        ));
        foreach ($gebiete_all->Gebiet as $gebiet){
            echo "{$gebiet->BEZEICHNUNG} \n";
            $anliegen_all = $client->getAnliegen();
            foreach ($anliegen_all as $li){
                foreach($li as $anliegen){
                    if($anliegen->FLAGPUBLIC){
                        $antragstellung = $client->getElektronischeAntragstellungLink(array(
                            "AnliegenID" => $anliegen->ID,
                            "GebietID" => $gebiet->ID
                        ));
                        if(isset($antragstellung)){
                            $test = get_object_vars($antragstellung);
                            $anzahl = count($test);
                            #var_dump ($anzahl);
                            if($anzahl > 0){
                                $name=$anliegen->BEZEICHNUNG;
                                $id=$anliegen->ID;
                                echo"has ElektronischeAntragstellung:\n{$name} {$id}\n";
                                var_dump($antragstellung);
                                #exit();
                            }
                        }
                        
                        /*if(isset($anliegen->DOKUMENT)){
                            if(!is_array($anliegen->DOKUMENT)){
                             $anliegen->DOKUMENT = array($anliegen->DOKUMENT);
                            }
                            foreach($anliegen->DOKUMENT as $dokument){
                            #echo "adresse \n";
                                if(isset($dokument->DOKUMENTSCHLUESSELID) /*and $gebuehr->VORKASSE == true*//*){
                                    $name=$anliegen->BEZEICHNUNG;
                                    $id=$anliegen->ID;
                                    echo"is DOKUMENTSCHLUESSELID {$name} {$id}";
                                    exit();
                                }
                            }
                        }*/
                    }
                }
            }
        }
        #var_dump($data);
        #return array(
        #'data' => $data
        #);
    }
}
