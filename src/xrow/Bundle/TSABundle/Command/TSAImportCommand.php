<?php
namespace xrow\Bundle\TSABundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use xrow\Bundle\TSABundle\TSAClient;

class TSAImportCommand extends ContainerAwareCommand
{
    #const AREA_ID_REGION_HANNOVER = 8662375; // Region Hannover 
    #const ANLIEGEN_ID_GEWERBE_ANMELDUNG = 8664844; // Gewerbeanmeldung
    #const OE_ID_LANDESAMT_SOZIALES = 8670016;
    #const TEST_OE = 9431119; // Architektenkammer Niedersachsen
    #const URL_DOKU = "http://ws-ni-schul.zfinder.de/doc/6_00/";

    protected function configure()
    {
        $this->setName('tsa:import')
            ->setDescription('This is a script to get TSA data');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        // var $location "Bürgen Service Node"
        $location = null;
        // var $contentType "Folder"
        $contentType = null;
        $source = new TSA\Source\Anliegenkategorien();
        $import = new Import\Process( $location, $contentType, $source );
        if( $import->validate() ){
            $import->import();
        }
        $source = new TSA\Source\SubAnliegenkategorien();
        $import = new Import\Process( $location, $contentType, $source );
        if( $import->validate() ){
            $import->import();
        }
        // var $contentType "Leistung"
        $contentType = null;
        $source = new TSA\Source\AnliegenRegional();
        $import = new Import\Process( $location, $contentType, $source );
        if( $import->validate() ){
            $import->import();
        }
        // var $location "Node Organisationen im Media Bereich"
        $location = null;
        // var $contentType "Organisation"
        $contentType = null;
        $source = new TSA\Source\Organisationseinheiten();
        $import = new Import\Process( $location, $contentType, $source );
        if( $import->validate() ){
            $import->import();
        }
        // var $location "Node Kontakte im Media Bereich"
        $location = null;
        // var $contentType "Kontakt"
        $contentType = null;
        $source = new TSA\Source\Person();
        $import = new Import\Process( $location, $contentType, $source );
        if( $import->validate() ){
            $import->import();
        }
        // var $location "Node Kontakte im Media Bereich"
        $location = null;
        // var $contentType "Kontakt"
        $contentType = null;
        $source = new TSA\Source\Form();
        $import = new Import\Process( $location, $contentType, $source );
        if( $import->validate() ){
            $import->import();
        }
    }
}
