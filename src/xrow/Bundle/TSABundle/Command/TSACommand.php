<?php
namespace xrow\Bundle\TSABundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use xrow\Bundle\TSABundle\TSAClient;

class TSACommand extends ContainerAwareCommand
{

    protected function configure()
    {
        $this->setName('tsa:demo')
        ->setDescription('This is a demo script to get some TSA demo data');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /*
         * $name = $input->getArgument('name'); if ($name) { $text = 'Hello '.$name; } else { $text = 'Hello'; } if ($input->getOption('yell')) { $text = strtoupper($text); }
        */
        try {
            // Create Client
            $client = new TSAClient();

            // Use Client
            $anliegen = $client->getAnliegen(array(
                "suchwort" => "Wohngeld",
                "Volltextsuche" => 0
            ));
            print_r($anliegen);
            $gebiete = $client->getGebiete();
            print_r($gebiete);

        } catch (\Exception $e) {
            echo "====== REQUEST HEADERS =====" . PHP_EOL;
            var_dump($client->__getLastRequestHeaders());
            echo "========= REQUEST ==========" . PHP_EOL;
            var_dump($client->__getLastRequest());
            echo "====== RESPONSE HEADERS =====" . PHP_EOL;
            var_dump($client->__getLastRequestHeaders());
            echo "========= RESPONSE =========" . PHP_EOL;
            var_dump($client->__getLastResponse());
            throw $e;
        }
        // Destroy Client
        unset($client);
    }
}
